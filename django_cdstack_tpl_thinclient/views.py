import base64

from Cryptodome.Cipher import DES3
from django.template import engines

import copy

from django_cdstack_deploy.django_cdstack_deploy.func import (
    generate_config_static,
    zip_add_file,
)
from django_cdstack_tpl_deb_buster.django_cdstack_tpl_deb_buster.views import (
    handle as handle_deb_buster,
)
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import handle as handle_deb
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import get_os_release
from django_cdstack_tpl_deb_stretch.django_cdstack_tpl_deb_stretch.views import (
    get_apt_packages,
    get_apt_sources,
    get_apt_prefs,
)
from django_cdstack_tpl_crowdsec_main.django_cdstack_tpl_crowdsec_main.views import (
    handle as handle_crowdsec_main,
)
from django_cdstack_tpl_fail2ban.django_cdstack_tpl_fail2ban.views import (
    handle as handle_fail2ban,
)
from django_cdstack_tpl_ipcop.django_cdstack_tpl_ipcop.views import (
    handle as handle_ipcop,
)
from django_cdstack_tpl_wazuh_agent.django_cdstack_tpl_wazuh_agent.views import (
    handle as handle_wazuh_agent,
)


def encrypt_remmina_password(plain: str, secret: str):
    plain = plain.encode("utf-8")
    secret_b64 = base64.b64decode(secret)
    key = secret_b64[:24]
    iv = secret_b64[24:]
    plain = plain + b"\0" * (8 - len(plain) % 8)
    cipher = DES3.new(key, DES3.MODE_CBC, iv)
    result = cipher.encrypt(plain)
    result = base64.b64encode(result)
    result = result.decode("utf-8")
    return result


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_thinclient/django_cdstack_tpl_thinclient"

    if "apt_packages" not in template_opts:
        template_opts["apt_packages"] = get_apt_packages(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "fail2ban_apps" not in template_opts:
        template_opts["fail2ban_apps"] = copy.deepcopy(template_opts["apt_packages"])

    if "apt_sources" not in template_opts:
        template_opts["apt_sources"] = get_apt_sources(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "apt_preferences" not in template_opts:
        template_opts["apt_preferences"] = get_apt_prefs(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "os_release" not in template_opts:
        template_opts["os_release"] = get_os_release(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    for key in template_opts.keys():
        if key.startswith("rds_conprof_") and key.endswith("_name"):
            key_ident = key[:-5]

            rds_connection_profile = dict()

            if (
                "rds_remmina_secret" in template_opts
                and key_ident + "_user_password_uenc" in template_opts
            ):
                rds_connection_profile["rds_user_password"] = encrypt_remmina_password(
                    template_opts[key_ident + "_user_password_uenc"],
                    template_opts["rds_remmina_secret"],
                )
            else:
                rds_connection_profile["rds_user_password"] = template_opts[
                    key_ident + "_user_password"
                ]

            rds_connection_profile["rds_connection_name"] = template_opts[
                key_ident + "_name"
            ]
            rds_connection_profile["rds_destination_server"] = template_opts[
                key_ident + "_destination_server"
            ]
            rds_connection_profile["rds_user_username"] = template_opts[
                key_ident + "_user_username"
            ]
            rds_connection_profile["rds_user_domain"] = template_opts[
                key_ident + "_user_domain"
            ]

            rds_connection_profile["rds_sound"] = template_opts[key_ident + "_sound"]

            if key_ident + "_sound" in template_opts:
                rds_connection_profile["rds_sound"] = template_opts[
                    key_ident + "_sound"
                ]
            else:
                rds_connection_profile["rds_sound"] = "off"

            if key_ident + "_gateway_server" in template_opts:
                rds_connection_profile["rds_gateway_server"] = template_opts[
                    key_ident + "_gateway_server"
                ]

            config_template_file = open(
                module_prefix
                + "/templates/config-fs/dynamic/home/user/.local/share/remmina/connection_rdp.remmina",
                "r",
            ).read()
            config_template = django_engine.from_string(config_template_file)

            zip_add_file(
                zipfile_handler,
                "home/user/.local/share/remmina/" + template_opts[key] + ".remmina",
                config_template.render(rds_connection_profile),
            )

    vpn_services = str()

    for key in template_opts.keys():
        if key.startswith("vpn_conprof_") and key.endswith("_name"):
            key_ident = key[:-5]

            vpn_connection_profile = dict()

            vpn_connection_profile["vpn_profile_name"] = template_opts[
                key_ident + "_name"
            ]
            vpn_connection_profile["vpn_profile_server"] = template_opts[
                key_ident + "_server"
            ]
            vpn_connection_profile["vpn_profile_port"] = template_opts[
                key_ident + "_port"
            ]
            vpn_connection_profile["vpn_profile_ca_cert"] = template_opts[
                key_ident + "_ca_cert"
            ]
            vpn_connection_profile["vpn_profile_cert"] = template_opts[
                key_ident + "_cert"
            ]
            vpn_connection_profile["vpn_profile_key"] = template_opts[
                key_ident + "_key"
            ]
            vpn_connection_profile["vpn_profile_cipher"] = template_opts[
                key_ident + "_cipher"
            ]

            if key_ident + "_compress_algo" in template_opts:
                vpn_connection_profile["vpn_profile_compress_algo"] = template_opts[
                    key_ident + "_compress_algo"
                ]

            conf_path = "etc/openvpn/client/"
            path = conf_path + vpn_connection_profile["vpn_profile_name"] + "/"

            config_template_file = open(
                module_prefix
                + "/templates/config-fs/dynamic/etc/openvpn/client/profile.conf",
                "r",
            ).read()
            config_template = django_engine.from_string(config_template_file)

            zip_add_file(
                zipfile_handler,
                conf_path + vpn_connection_profile["vpn_profile_name"] + ".conf",
                config_template.render(vpn_connection_profile),
            )

            config_template_file = open(
                module_prefix
                + "/templates/config-fs/dynamic/etc/openvpn/client/profile/profile-ca.crt",
                "r",
            ).read()
            config_template = django_engine.from_string(config_template_file)

            zip_add_file(
                zipfile_handler,
                path + vpn_connection_profile["vpn_profile_name"] + "-ca.crt",
                config_template.render(vpn_connection_profile),
            )

            config_template_file = open(
                module_prefix
                + "/templates/config-fs/dynamic/etc/openvpn/client/profile/profile.crt",
                "r",
            ).read()
            config_template = django_engine.from_string(config_template_file)

            zip_add_file(
                zipfile_handler,
                path + vpn_connection_profile["vpn_profile_name"] + ".crt",
                config_template.render(vpn_connection_profile),
            )

            config_template_file = open(
                module_prefix
                + "/templates/config-fs/dynamic/etc/openvpn/client/profile/profile.key",
                "r",
            ).read()
            config_template = django_engine.from_string(config_template_file)

            zip_add_file(
                zipfile_handler,
                path + vpn_connection_profile["vpn_profile_name"] + ".key",
                config_template.render(vpn_connection_profile),
            )

            vpn_services += (
                "systemctl enable openvpn-client@"
                + vpn_connection_profile["vpn_profile_name"]
                + "\n"
            )

    template_opts["vpn_service_enable"] = vpn_services

    generate_config_static(zipfile_handler, template_opts, module_prefix)
    handle_ipcop(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_fail2ban(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_crowdsec_main(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_wazuh_agent(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    if not skip_handle_os:
        handle_deb(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    return True
