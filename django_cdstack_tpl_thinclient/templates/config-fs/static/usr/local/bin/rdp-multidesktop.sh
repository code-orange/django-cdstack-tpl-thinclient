#!/bin/bash

. /home/user/.config/thinclient/vars

if test $rem_user; then
         if test ! $rem_pw; then
                input=`zenity --password --title "Password"`
                rem_pw=$input
        fi
else
        input=`zenity --password --username --title "Password"`
        rem_user=`echo $input | cut -d "|" -f1`
        rem_pw=`echo $input | cut -d "|" -f2`

fi

if test ! $rem_server; then
        zenity --warning --text="no server specified"
else
        xfreerdp /multimon /cert-ignore /drive:USB,/media/user /f /smartcard /auto-reconnect /auto-reconnect-max-retries:10{% if rds_conprof_01_sound != "off" %} /audio-mode:0 /sound:sys:alsa{% endif %} /v:$rem_server /u:$rem_user /d:$rem_domain /p:$rem_pw
        exit_code=$?
        echo $exit_code
        case $exit_code in
                11)
                        ;;
                12)
                        ;;
                255)
                        ;;
                131)
                zenity --warning --text="wrong password"
                ;;
                *)
                        zenity --warning --text="error connecting"
                        ;;
        esac
fi
