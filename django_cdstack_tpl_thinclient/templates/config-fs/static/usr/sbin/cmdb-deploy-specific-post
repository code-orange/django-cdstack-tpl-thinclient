#!/bin/sh

export DEBIAN_FRONTEND=noninteractive
export APT_LISTCHANGES_FRONTEND=none
export OSSEC_ACTION_CONFIRMED=y

# Mark etc as safe directory for git
git config --global --add safe.directory /etc

# Modify system environment
chmod 644 /etc/krb5.conf
chmod 644 /etc/network/interfaces
chmod 644 /etc/network/interfaces.new
chmod 644 /etc/network/interfaces.d/*
chmod -R +rx /etc/network/if-up.d/*
chmod -R +rx /etc/network/if-pre-up.d/*
chmod -R +rx /etc/network/if-post-down.d/*
chmod -R +rx /etc/network/if-down.d/*
chmod +rx /etc/environment
chmod +x /etc/profile.d/*
chmod -R g+r,o+r /etc/hosts
chmod -R g+r,o+r /etc/nsswitch.conf
chmod -R g+r,o+r /etc/resolv.conf
chmod -R g+r,o+r /etc/ntp.conf
chmod -R g+r,o+r /etc/systemd/timesyncd.conf
chmod -R g+r,o+r /etc/chrony/chrony.conf

# Set correct permissions on service defaults
chmod -R +r /etc/default/*

# Update grub and kernel params
update-grub2

# Update CMDB client
cp /opt/cdstack-cmdb-deploy/files/usr/sbin/cmdb-deploy /usr/sbin/cmdb-deploy
chown root:root /usr/sbin/cmdb-deploy
chown root:root /usr/sbin/cmdb-deploy-dev
chmod 770 /usr/sbin/cmdb-deploy
chmod 770 /usr/sbin/cmdb-deploy-dev

# Set cron owner
chown root:root /etc/cron.d/codeorange-cmdb
chmod 0644 /etc/cron.d/codeorange-cmdb

# APT: Set owner and sync packages if enabled
chown -R _apt:root /etc/apt
chmod -R +r /etc/apt/apt.conf.d
chmod +r /etc/apt/sources.list
chmod -R +r /etc/apt/sources.list.d
chmod -R +r /etc/apt/preferences.d
chmod +r /etc/apt/trusted.gpg
chmod -R +r /etc/apt/trusted.gpg.d
{% if cmdb_host.pkg_sync_enable == 1 %}
# Sync packages from image
apt-get update --allow-releaseinfo-change
apt-get install --fix-broken --assume-yes
apt-get purge --yes --force-yes $(cat /etc/cmdb/apt_packages_purge.txt)
apt-get install --yes -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --force-yes --no-upgrade --ignore-missing $(cat /etc/cmdb/apt_packages.txt)
{% endif %}

x11vnc -storepasswd {{ vnc_password }} /etc/X11/x11vnc.passwd

usermod -a -G plugdev user

mkdir -p /media/user
mkdir -p /home/user/.config/autostart/

chmod 644 /usr/share/applications/RDP-Multidesktop.desktop
chmod 644 /usr/share/applications/Shutdown.desktop
chmod 644 /usr/share/applications/Reboot.desktop

{% if lab_feature_multimon == 'true' %}
    ln -s /usr/share/applications/RDP-Multidesktop.desktop /home/user/.config/autostart/
    ln -s /usr/share/applications/RDP-Multidesktop.desktop /home/user/Desktop/
{% else %}
    ln -s /usr/share/applications/org.remmina.Remmina.desktop /home/user/.config/autostart/
    ln -s /usr/share/applications/org.remmina.Remmina.desktop /home/user/Desktop/
{% endif %}

ln -s /usr/share/applications/Shutdown.desktop /home/user/Desktop/
ln -s /usr/share/applications/Reboot.desktop /home/user/Desktop/

chmod 777 /home/user/.xsessionrc
chmod 555 /usr/local/bin/rdp-multidesktop.sh
chmod 555 /usr/local/bin/shutdown.sh
chmod 555 /usr/local/bin/reboot.sh

# Make sure the user has permissions on his own folder
chown -R user:user /home/user

# Reload Service Config
systemctl daemon-reload

# WAZUH/OSSEC - security monitoring solution for threat detection, integrity monitoring, incident response and compliance
{% if monitoring_wazuh_agent_key %}
/var/ossec/bin/manage_agents -i "{{ monitoring_wazuh_agent_key }}"
chown -R ossec:ossec /var/ossec/
systemctl enable wazuh-agent.service
{% endif %}

# Basic System Services
chown -R mail:mail /etc/dma/
chmod +r /etc/postfix/master.cf
chmod +r /etc/postfix/main.cf
chmod 644 /etc/mailname
chmod 644 /etc/aliases
systemctl enable zramswap
systemctl enable haveged
systemctl enable sshd
systemctl enable ntp || systemctl enable chrony
systemctl enable lldpd
systemctl enable snmpd

# System Monitoring
chown -R nagios:nagios /etc/icinga2
chown -R nagios:nagios /var/lib/icinga2
systemctl enable icinga2
systemctl enable salt-minion
systemctl enable smartd
systemctl enable watchdog
systemctl disable corosync
systemctl disable pacemaker
sensors-detect --auto

# ThinClient services
systemctl enable smbd
systemctl enable x11vnc
systemctl enable udisks2

# VPN services
{{ vpn_service_enable }}

# SmartCard
systemctl enable pcscd

# Filesystem Maintenance
systemctl enable fstrim.timer

# Security and Firewall
systemctl unmask shorewall
systemctl enable shorewall
systemctl unmask shorewall6
systemctl enable shorewall6

{% if fail2ban_apps %}sh /usr/local/sbin/fail2ban_touch_logs.sh{% endif %}
systemctl unmask fail2ban
systemctl enable fail2ban
systemctl enable crowdsec.service
systemctl enable crowdsec-firewall-bouncer.service


etckeeper commit "cmdb-deploy-specific-post" || true
systemctl enable etckeeper.service etckeeper.timer
